import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { normalizeW, normalizeH } from '../functions/global'
import Icon from 'react-native-vector-icons/AntDesign'
export default function CartItem(props) {
    console.log(props)
    return (
        <View style={{justifyContent:'center',alignItems:'center',marginBottom:normalizeH(15)}}>
            <View style={{width:'90%',height:normalizeH(150),elevation:1,borderRadius:10,justifyContent:'center'}}>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                    <Image source={{uri: 'http://192.168.0.12:3001/'+props.img}}
                    style={{width:normalizeW(100),height:normalizeH(100),resizeMode:'cover',marginLeft:normalizeW(15),borderRadius:10}}
                    />
                    <View style={{alignItems:'flex-start',justifyContent:'flex-start',backgroundColor:'#fff', marginLeft:10}}>
                        <Text numberOfLines={2} style={{fontFamily:'Roboto',fontSize:normalizeW(20),width:'70%'}}>{props.title}</Text>
                        <Text style={{fontSize:normalizeW(20),marginTop:normalizeH(5),fontFamily:'RobotoLight'}}>{props.sidesqty}x{props.sidesname}  {props.drinksqty}x{props.drinksname}</Text>
                        <Text style={{fontFamily:'RobotoLight',fontSize:normalizeW(28),marginTop:normalizeH(10)}}>${props.price}.00 </Text>
                    </View>
                    <View style={{position:'absolute',right:0,marginRight:10,flexDirection:'row', alignItems:'center',alignSelf:'flex-end',justifyContent:'center',}}>
                        <TouchableOpacity activeOpacity={.85} onPress={()=>alert('Subtract')}>
                            <Icon name="minuscircleo" size={32} color='#444444'/>
                        </TouchableOpacity>
                        <Text style={{fontFamily:'RobotoLight',fontSize:normalizeW(36), marginHorizontal:normalizeW(12)}}>{props.qty}</Text>
                        <TouchableOpacity activeOpacity={.85} onPress={()=>alert('Plus')}>
                            <Icon name="pluscircleo" size={32} color='#E9687E'/>
                        </TouchableOpacity>
                    </View>
                    <View style={{position:'absolute',top:-5,right:0,backgroundColor:'#000',borderBottomLeftRadius:5,borderTopLeftRadius:5}}>
                        <Text style={{color:'#fff',fontSize:normalizeW(16), marginHorizontal:normalizeW(12),paddingHorizontal:normalizeW(8),paddingVertical:normalizeH(4)}}>
                        {props.restaurant}
                        </Text>
                    </View>
                </View>
            </View>
        </View>
    )
}
