import React from 'react'
import { StyleSheet, Image, TouchableOpacity, View } from 'react-native'
import { useNavigation } from '@react-navigation/native'

const BackNav = (props) => {
    const navigation = useNavigation();
    return (
        <View>
        <View style={{width:'50%'}}>
            <TouchableOpacity activeOpacity={0.55} onPress={() => navigation.goBack()}>
                    <Image
                        source={require('../assets/images/BackButton.png')}
                    />
                </TouchableOpacity>
                </View>
        </View>
    )
}

export default BackNav

const styles = StyleSheet.create({})
