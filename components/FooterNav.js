import React from 'react'
import { View, Text } from 'react-native'
import { normalizeH, normalizeW } from '../functions/global'
import AntDesignI from 'react-native-vector-icons/AntDesign'
import EntypoI from 'react-native-vector-icons/Entypo'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { useNavigation } from '@react-navigation/native'
import { useNavigationState } from '@react-navigation/native';

const FooterNav = (props) => {
    const navigation = useNavigation();
    const pageName = useNavigationState(state => state.routes[state.index].name);
    let active = ""
    switch(pageName){
        case "Home":
            active = "Home"
            break;
        case "Orders":
            active = "Orders"
            break;
        case "Favorites":
            active = "Favorites"
            break;
        default:
            active = ""
    }

    return (
        <View style={{flex:1, backgroundColor:'transparent'}}>

        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around' , alignItems: 'center', backgroundColor: '#000', borderTopLeftRadius: normalizeH(75),overflow:'hidden' }}>

            <View style={{ flex: 1, marginLeft: normalizeW(20), justifyContent: 'center' , alignItems: 'center',}}>
                <TouchableOpacity activeOpacity={0.85} onPress={() => navigation.navigate('Home')} >
                    <View style={{}}>
                        <AntDesignI name="home" color="#FFF" size={24} style={{textAlign:'center', color:active=="Home"?"#F9E197":"#fff"}} />
                        <Text style={{ color:active=="Home"?"#F9E197":"#fff" }}>Home</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{ flex: 1, justifyContent: 'center' , alignItems: 'center'  }}>
                <TouchableOpacity activeOpacity={0.85} onPress={() => navigation.navigate('Orders')} >
                    <View>
                        <AntDesignI name="wallet" color="#FFF" size={24} style={{textAlign:'center', color:active=="Orders"?"#F9E197":"#fff"}} />
                        <Text style={{ color:active=="Orders"?"#F9E197":"#fff" }}>Orders</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{ flex: 1, justifyContent: 'center' , alignItems: 'center'  }}>
                <TouchableOpacity activeOpacity={0.85} onPress={() => alert('Feature coming soon')} >
                    <View>
                        <EntypoI name="heart-outlined" color="#FFF" size={24} style={{textAlign:'center', color:active=="Favorites"?"#F9E197":"#fff"}} />
                        <Text style={{ color:active=="Favorites"?"#F9E197":"#fff" }}>Favorites</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
        </View>
    )
}

export default React.memo(FooterNav);
