import React from 'react'
import { View, Text } from 'react-native'
import { normalizeW, normalizeH } from '../functions/global'



export default function ChargeItem(props) {
    

    return (
        <View style={{flexDirection: 'row',justifyContent:'center',alignItems:'space-between',marginBottom:normalizeH(10) }}>
            <View style={{flex:1,justifyContent:'center',alignSelf:'flex-end'}}>
                <Text style={{fontSize:normalizeW(32),marginTop:normalizeH(-60),textAlign:'center',fontFamily:'RobotoThin'}}>
                    1 x
            </Text>
            </View>
            <View style={{flex:2,alignSelf:'flex-end',alignItems:'flex-start'}}>
                <Text style={{fontSize:normalizeW(20),fontFamily:'RobotoLight'}}>
                    {props.title}
                </Text>
                {props.restaurantname != 'Passchru' ?
                <Text style={{fontSize:normalizeW(16),fontFamily:'Roboto'}}>
                    {props.sideqty} x {props.sidesname}, {props.drinkqty} x {props.drinksname}
                    
                </Text> 
                :
                <Text style={{fontSize:normalizeW(16),fontFamily:'Roboto'}}>
                    Transaction fees
                    
                </Text>}
            </View>
            <View style={{flex:2,alignItems:'flex-end'}}>
                <Text style={{fontSize:normalizeW(28),fontFamily:'Roboto'}}>
                    ${props.price}.00
            </Text>
                <View style={{backgroundColor:'#FE5959',width:'50%',borderRadius:30,padding:2}}>
                <Text style={{fontSize:normalizeW(14),fontFamily:'Roboto',color:'#fff',textAlign:'center'}}>{props.restaurantname}</Text>
                </View>
            </View>
        </View>
    )
}
