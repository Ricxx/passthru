import React from 'react'
import { View, Text, Image } from 'react-native'
import { normalizeW,normalizeH } from '../functions/global'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { useNavigation } from '@react-navigation/native';

export default function ProductThumb(props) {

    const navigation = useNavigation();
    let imageUri = props.image
    //style={{width:150,height:150}} props.foodImage
    return (
        <View style={{backgroundColor:'#fAfAfA',
            marginLeft:15,
            borderRadius:20, 
            width:normalizeW(234),
            shadowColor: "#000",
            // shadowOffset: {width: 10, height: 10},
            // shadowOpacity: 1,
            // shadowRadius: 8.00,
            borderBottomWidth: 0,
             elevation: 2.2,
            
        }}>
            <View>
                <Image source={{uri: 'http://192.168.0.12:3001/'+props.image}} 
                    style={{borderRadius:10, resizeMode:'cover', width:normalizeW(234),height:normalizeH(132)}}
                />
            </View>
            <View style={{padding:10,marginBottom:40}}>
                <Text style={{fontSize:normalizeW(22),fontFamily:'RobotoLight',paddingVertical:normalizeH(4)}}>{props.name}</Text>
                <Text style={{fontSize:normalizeW(16),fontFamily:'Roboto',color:'#333',paddingVertical:1}}>{props.desc}</Text>
                <Text style={{fontSize:normalizeW(24),fontFamily:'RobotoLight',paddingVertical:2}}>${props.price}.00</Text>
            </View>
            <View style={{position:'absolute',bottom:0,right:0,marginTop:normalizeH(30),backgroundColor:'#000', borderBottomEndRadius:20, borderTopLeftRadius:30, width:'40%', alignSelf:'flex-end', alignItems:'center',}}>
            <TouchableOpacity  style={{padding:10,paddingHorizontal:normalizeW(30)}} 
                onPress={()=>navigation.navigate('Product', {itemId: props.id})}
            >
            <View style={{flex:1,width:'100%',}}>
                <Image source={require('../assets/images/cartButtonFood.png')}  />
            </View>
            </TouchableOpacity>
            </View>
        </View>
    )
}
