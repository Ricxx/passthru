import React,{ useState } from 'react'
import { View, Text, Image } from 'react-native'
import { normalizeH, normalizeW } from '../functions/global'
import DateTimePicker from '@react-native-community/datetimepicker';
import { TouchableOpacity } from 'react-native-gesture-handler';
import moment from 'moment'

export default function PickUp(props) {
    const [show, setShow] = useState(false);
    const [dates,setDates] = useState({});

    const updateDate = (val) => {
        setShow(false);
        props.setPickup(val.nativeEvent.timestamp)
        console.log(val.nativeEvent.timestamp);
    }
    return (
        <View>
            <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'center', marginTop: normalizeH(30) }}>
                <Image source={require('../assets/images/paymentmethodicon.png')} style={{ width: 48, height: 48 }} />
                <Text style={{ fontSize: normalizeW(32), fontFamily: 'RobotoLight', marginLeft: normalizeW(12) }}>Schedule PickUp(Optional)</Text>
            </View>
            <View style={{ marginLeft: normalizeW(50), marginVertical: normalizeH(20) }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly' }}>
                    <Text>FROMAGE</Text>
                    <Text style={{ fontSize: normalizeW(20) }}>{props.pickup== 0 ? 
                            "- - : - -" :
                            moment(props.pickup).format('LT')
                    }</Text>
                    <TouchableOpacity onPress={() => setShow(true)}><View style={{ backgroundColor: '#000', paddingHorizontal: normalizeW(8), paddingVertical: normalizeH(3), borderRadius: 20 }}><Text style={{ color: '#fff' }}>Set schedule</Text></View></TouchableOpacity>
                </View>
                {show && <DateTimePicker
                    minuteInterval={10}
                    value={new Date()}
                    mode="time"
                    display="spinner"
                    is24Hour={true}
                    onChange={(value)=>updateDate(value)}
                />}
            </View>
        </View>
    )
}
