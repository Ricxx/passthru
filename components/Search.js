import React from 'react'
import { View, Image, TextInput } from 'react-native'
import { normalizeH,normalizeW } from '../functions/global'

export default function Search() {
    const [value, onChangeText] = React.useState('Search...');
    return (
        <View>
        <View>
                <TextInput 
                style={{ height: normalizeH(35), width: '65%', position:'absolute',zIndex:3,backgroundColor:'#fff',top:normalizeH(22),left:normalizeW(62), borderColor: '#F9E197', borderWidth: 2, borderRadius:30,paddingLeft:normalizeW(10), color:'#555555' }}
      onChangeText={text => onChangeText(text)}
      value={value}
       />
            </View>
            <View>
                <Image
                            source={require('../assets/images/searchBtn.png')}
                />
            </View>
            
        </View>
    )
}
