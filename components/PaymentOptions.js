import React from 'react'
import { View, Text, Image } from 'react-native'
import { normalizeH, normalizeW } from '../functions/global'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button'

export default function PaymentOptions(props) {
    
    const radio_props1 = [{label: 'Credit Card', value: 0 },
    {label: 'Account balance', value: 1 },
    {label: 'Cash on Pick-Up', value: 2 },]
    return (
        <View>
        <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'center', marginTop: normalizeH(30) }}>
            <Image source={require('../assets/images/paymentmethodicon.png')} style={{ width: 48, height: 48 }} />
            <Text style={{ fontSize: normalizeW(32), fontFamily: 'RobotoLight', marginLeft: normalizeW(12) }}>Payment Method</Text>
        </View>
        <View style={{marginLeft:normalizeW(50),marginVertical:normalizeH(20)}}>
            <RadioForm
                radio_props={radio_props1}
                initial={props.payment}
                formHorizontal={false}
                labelHorizontal={true}
                buttonColor={'#c4c4c4'}
                selectedButtonColor={'#E9687E'}
                animation={true}
                labelStyle={{fontSize: 20, color: '#333333'}}
                onPress={(value) => {props.setPayment(value)}}
                />
            </View>
            </View>
    )
}
