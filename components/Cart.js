import React, {useContext,useEffect} from 'react'
import { View, Text, Image, ScrollView,StyleSheet,Modal,TouchableOpacity } from 'react-native'
import { normalizeH, normalizeW } from '../functions/global'
//import {  } from 'react-native-gesture-handler'
import { setCustomText } from 'react-native-global-props';
import CartItem from './CartItem'
import {CartContext} from '../context/CartContext'
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { useNavigation } from '@react-navigation/native'

const customTextProps = { 
  style: { 
    fontFamily: "Roboto"
  }
}

export default function Cart(props) {
    setCustomText(customTextProps);
    const [ cartStyle, setCartStyle ] = React.useState({transform: [{ scale: 0 }], zIndex: -1, justifyContent:'space-evenly', borderRadius: 15, shadowColor: '#000', shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 3,  position: 'absolute', top: normalizeH(-20), flex: 1, alignSelf: 'flex-end', width: '120%', height: normalizeH(0), backgroundColor: '#fff' })
    const [ modalVisible, setModalVisible ] = React.useState(false);
    const cartItems = useContext(CartContext)();
    const [ items, setItems ] = React.useState({ready:false,data:''})
    const navigation = useNavigation();

    async function getCart(){
        try {
          const jsonValue = await AsyncStorage.getItem('userCart')
          //console.log('retreivedCart: ',jsonValue)
          return jsonValue != null ? setItems({ready:true,data:JSON.parse(jsonValue)}) : null
        } catch(e) {
          console.log('Failed to load cart.', e)
        }
      }

    // useEffect(() => {
        
        
    // }, [cartItems.item])

    const clearCart = () =>{
        cartItems.cartFunctions().clearCart();
        setItems({ready:false,data:''});
    }
    const openCart = () => {
        //setCartStyle({position:'absolute', top:0,right:0,transform: [{ scale: 1 }], zIndex: 4, justifyContent:'space-evenly', borderRadius: 15, borderTopRightRadius:0, shadowColor: '#000', shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 3,  position: 'absolute', top: normalizeH(-20), flex: 3, alignSelf: 'flex-end', width: '120%', height: normalizeH(400), backgroundColor: '#fff' })
        getCart()
        setModalVisible(true);
    }
    const closeCart = () => {
        //setCartStyle({transform: [{ scale: 0 }], zIndex: -1, justifyContent:'space-evenly', borderRadius: 15, shadowColor: '#000', shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 3,  position: 'absolute', top: normalizeH(-20), flex: 1, alignSelf: 'flex-end', width: '120%', height: normalizeH(0), backgroundColor: '#fff' })
        setModalVisible(false);
    }
    return (
        <>
            <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', zIndex: 3 }}>
                <TouchableOpacity activeOpacity={0.75} onPress={() => openCart()}>
                    <Image
                        source={require('../assets/images/Cart.png')}
                    />
                </TouchableOpacity>
            </View>

            {/* Modify transform = 1/0 & zIndex4/2 & height = 500?  to show/hide component */}
            <View style={cartStyle}>
                
                {/* Cart closebutton and title */}
                <Modal 
                animationType="slide"
                onRequestClose={()=>closeCart()}
                visible={modalVisible}>
                <View style={{flex:1}}>
                <View style={{ alignSelf: 'center' }}>
                    <Image style={{ marginTop: normalizeH(10) }}
                        source={require('../assets/images/cartTitle.png')}
                    />
                </View>
                <View style={{ position: 'absolute', right: normalizeW(5), top: normalizeH(5) }}>
                    <TouchableOpacity activeOpacity={0.85} onPress={()=>closeCart()}>

                    <Image style={{ marginTop: normalizeH(0) }}
                        source={require('../assets/images/cartClose.png')}
                    />
                    </TouchableOpacity>
                </View>
                </View>

                {/* Cart items list view, render list of cart items */}
                <View style={{flex:6}}>
                
                <ScrollView style={{flex:5,overflow:'scroll',height:'100%'}}>
                {items.ready && items.data.length > 0 && items.data.map((item,idx)=>
                    {return (
                    <CartItem key={idx} id={item.foodid} title={item.title} img={item.image} price={item.price} sides={item.side}
                            drinks={item.drink} sidesqty={item.sideqty} drinksqty={item.drinkqty} 
                            restaurant={item.restaurantname} drinksname={item.drinksname} sidesname={item.sidename} restaurantID={item.restaurantid} qty={1}    
                            />
                )}
                )
                }

                <View style={{alignSelf:'flex-start',borderRadius:20,backgroundColor:'#f00',marginLeft:normalizeW(25)}}>
                <TouchableOpacity activeOpacity={0.85} onPress={clearCart}>
                <View style={{paddingVertical:6,paddingHorizontal:20,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                <Icon name="delete-sweep-outline" size={20} color='#fff'/><Text style={{color:'#fff',fontSize:20}}> Clear Cart</Text>
                </View>
                </TouchableOpacity>
                </View>

                </ScrollView>
                
                </View>

                {/* Cart checkout button */}
                
                <View style={{flex:1}}>
                <View style={{position:'absolute',bottom:0,right:0,justifyContent:'center',alignItems:'center',backgroundColor:'#000',borderBottomRightRadius:0,borderTopLeftRadius:normalizeW(90),width:'65%',height:'85%'}}>
                <TouchableOpacity activeOpacity={.85} onPress={()=>{navigation.navigate('Checkout');closeCart()}}>
                <View style={{paddingHorizontal:normalizeW(40),flex:1,width:'130%',justifyContent:'center'}}>
                    <Text style={{color:'#fff',fontSize:normalizeW(22)}}>Checkout</Text>
                </View>
                </TouchableOpacity>
                </View>
                </View>
                </Modal>

            </View>
        </>
    )
}
