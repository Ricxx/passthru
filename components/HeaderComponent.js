//conditionally renders cart + search or cart + back
import React from 'react'
import { View, StyleSheet } from 'react-native'
import Cart from '../components/Cart'
import BackButton from '../components/BackNav'
import Search from '../components/Search'
import { normalizeW } from '../functions/global'
import { useNavigationState } from '@react-navigation/native';

function HeaderComponent(props) {
    const navigation = useNavigationState(state => state.routes[state.index].name);
    //const navigation = useNavigation();
    console.log(navigation)
    if (navigation == 'Home')
        return (
            <View style={styles.headerView}>
                <View style={{}}>
                    <Cart />
                </View>
                <View style={{flex:1,marginLeft:normalizeW(10)}}>
                    <Search />
                </View>

            </View>
        )
    else
        return (
            <View style={styles.headerView}>
                <View style={{flex:1}}>
                    <Cart />
                </View>
                <View style={{flex:1,marginLeft:normalizeW(10)}}>
                    <BackButton />
                </View>
            </View>)
}

const styles = StyleSheet.create({
headerView: {
flexDirection: 'row-reverse',
width:'100%',
justifyContent:'space-between',
alignItems:'center',
}
});

export default React.memo(HeaderComponent);