import React, { useEffect, useState } from 'react'
import { View, Text, Image, ScrollView } from 'react-native'
import { normalizeW, normalizeH } from '../functions/global'
import ProductThumb from './ProductThumb';

const ProductRoll = (props) => {
    let catIco;
    //let dataSource;
    const [dataSource, setdataSource] = useState({loading:false,data:{}});
    let x = dataSource.data;
    catIco = require('../assets/images/bestsellerico.png');
    //console.log(x);
    useEffect(() => {
       // console.log('useEffect')
        async function fetchData(){
            //let data = await fetch('http://192.168.0.12:3001/kfcFoodMenu')
            let data = await fetch('http://192.168.0.12:3001/food')
            data = await data.json()
            console.log(data)
                setdataSource({loading:true,data:data})
              //  console.log(dataSource)
        }
        fetchData();
        // switch (props.ico) {
        //     case 'bseller':
                
        //         break;
        //     case 'trend':
        //         catIco = require('../assets/images/trendico.png');
        //         //dataSource = await fetch('http://192.168.0.9:3001/kfcFoodMenu');
        //         //dataSource = await dataSource.text();
        //         break;
        //     case 'newItems':
        //         catIco = require('../assets/images/newico.png');
        //        // dataSource = await fetch('http://192.168.0.9:3001/kfcFoodMenu');
        //         //dataSource = await dataSource.text();
        //         break;
        //     default:
        //         catIco = require('../assets/images/bestsellerico.png');
        //         //dataSource = await fetch('http://192.168.0.9:3001/kfcFoodMenu');
        //         //dataSource = await dataSource.text();
        //         break;
        //     }
        
    }, [])
   { 
    // const bsellers = [{
    //     id: 'kfcPop1',
    //     name: 'Popcorn Chicken',
    //     desc: '2 lg Fries - 2L Soda - 2 Biscuits',
    //     price: 1920,
    //     foodImage: require('../assets/food/kfcPopcornSquare.png')
    // }, {
    //     id: 'kfcPop2',
    //     name: 'Popcorn Chicken',
    //     desc: '3 lg Fries - 7L Soda - 2 Biscuits',
    //     price: 1920,
    //     foodImage: require('../assets/food/kfcPopcornSquare.png')
    // }, {
    //     id: 'kfcPop3',
    //     name: 'Popcorn Chicken',
    //     desc: '4 lg Fries - 1L Soda - 7 Biscuits',
    //     price: 1920,
    //     foodImage: require('../assets/food/kfcPopcornSquare.png')
    // }, {
    //     id: 'kfcPop4',
    //     name: 'Popcorn Chicken',
    //     desc: '5 lg Fries - 4L Soda - 3 Biscuits',
    //     price: 1920,
    //     foodImage: require('../assets/food/kfcPopcornSquare.png')
    // }];
       // const trend = [{
    //     id: 'foodplaceBurger1',
    //     name: 'Bistro Burger',
    //     desc: '2 lg Fries - 2L Soda - 2 Biscuits',
    //     price: 850,
    //     foodImage: require('../assets/food/foodplaceBurgerSquare.png')
    // }, {
    //     id: 'foodplaceBurger2',
    //     name: 'Bistro Burger',
    //     desc: '3 lg Fries - 7L Soda - 2 Biscuits',
    //     price: 870,
    //     foodImage: require('../assets/food/foodplaceBurgerSquare.png')
    // }, {
    //     id: 'foodplaceBurger3',
    //     name: 'Bistro Burger',
    //     desc: '4 lg Fries - 1L Soda - 7 Biscuits',
    //     price: 890,
    //     foodImage: require('../assets/food/foodplaceBurgerSquare.png')
    // }, {
    //     id: 'foodplaceBurger4',
    //     name: 'Bistro Burger',
    //     desc: '5 lg Fries - 4L Soda - 3 Biscuits',
    //     price: 900,
    //     foodImage: require('../assets/food/foodplaceBurgerSquare.png')
    // }];

    // const newItems = [{
    //     id: 'kfcBucket1',
    //     name: 'Bucket Chicken Deal',
    //     desc: '2 lg Fries - 2L Soda - 2 Biscuits',
    //     price: 1220,
    //     foodImage: require('../assets/food/kfcBucketSquare.png')
    // }, {
    //     id: 'kfcBucket2',
    //     name: 'Bucket Chicken Deal',
    //     desc: '3 lg Fries - 7L Soda - 2 Biscuits',
    //     price: 1420,
    //     foodImage: require('../assets/food/kfcBucketSquare.png')
    // }, {
    //     id: 'kfcBucket3',
    //     name: 'Bucket Chicken Deal',
    //     desc: '4 lg Fries - 1L Soda - 7 Biscuits',
    //     price: 1620,
    //     foodImage: require('../assets/food/kfcBucketSquare.png')
    // }, {
    //     id: 'kfcBucket4',
    //     name: 'Bucket Chicken Deal',
    //     desc: '5 lg Fries - 4L Soda - 3 Biscuits',
    //     price: 2120,
    //     foodImage: require('../assets/food/kfcBucketSquare.png')
    // }]
}

    

    return (
        <View>

            {/* Title */}
            <View style={{ marginTop: normalizeH(20), marginLeft: normalizeW(18) }}>
                <View style={{ flexDirection: 'row' }}>
                    <Image source={catIco} style={{ width: normalizeW(36), height: normalizeW(36) }} />
                    <Text style={{ fontSize: normalizeW(24), fontFamily: 'RobotoLight' }}>{props.type}</Text>
                </View>
                <Image source={require('../assets/images/dividerHome.png')} style={{ marginTop: normalizeW(6), marginLeft: normalizeW(6) }} />

            </View>

            {/* Product component */}
            <ScrollView>
                {/* Get data and render each item */}
                <ScrollView horizontal={true} style={{ flex: 1,marginTop:normalizeH(10), marginRight:normalizeW(15),paddingBottom:normalizeH(15) }}>
                    {dataSource.loading && x.map((food) => {
                        return (
                            <ProductThumb
                                key={food.id}
                                id={food.id}
                                name={food.title}
                                desc={food.description}
                                price={food.price}
                                image={food.imagename}
                            />
                        )
                    })}
                </ScrollView>

            </ScrollView>
        </View>
    )
}

export default ProductRoll