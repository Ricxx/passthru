import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign'
import { normalizeW, normalizeH } from '../functions/global'
import { useNavigation } from '@react-navigation/native';
import moment from 'moment'

export default function HistoryItem(props) {
    //const navigation = useNavigation();
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    return (
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', backgroundColor:'#F5F5F5', borderRadius:20,marginBottom:20,width:'90%', elevation:1, alignSelf:'center'}}>
            <View style={{ alignItems: 'center',padding:10 }}>
                <Icon name="clockcircleo" size={normalizeW(36)} color="#FE5959" style={{marginBottom:normalizeH(10)}}/>
                <Text>{moment(props.orderTime*1000).format('L')}</Text>
                <Text>{moment(props.orderTime*1000).format('LT')}</Text>
            </View>
            <View style={{alignItems:'flex-start',padding:20}}>
                <Text style={{fontSize:normalizeW(16)}}>Order#: {props.orderId}</Text>
                <Text style={{fontSize:normalizeW(14)}}>{props.restaurant} - ${numberWithCommas(props.total)}</Text>
                <View style={{marginTop:10,paddingHorizontal:normalizeW(10),paddingVertical:normalizeH(3),borderRadius:20,backgroundColor:props.orderStatus=="Completed" ? '#59a359': '#FE5959'}}>
                <Text style={{color:'#fff'}}>{props.orderStatus}</Text>
                </View>
            </View>

        <View style={{ alignSelf: 'flex-end', backgroundColor: '#000',borderBottomEndRadius:20,borderTopLeftRadius:40 }}>
        <TouchableOpacity activeOpacity={0.85} onPress={()=>props.nav(props.orderId,props.restaurant,props.total,props.pin,props.conf,props.orderTime,props.orderStarted,props.orderCompleted)}>
                <View style={{paddingVertical:normalizeH(10),paddingHorizontal:normalizeW(20)}}>
                    <Text style={{ color: '#fff',fontSize:normalizeW(16) }}>View details</Text>
                </View>    
            </TouchableOpacity>
        </View>
            
        </View>
    )
}
