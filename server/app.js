const express = require('express');
const mysql = require('mysql');
const app = express();
var path = require('path');
const bodyparser = require('body-parser');
var cors = require('cors');

app.use(cors())
app.use(bodyparser.json());



//Create Connection
const db = mysql.createConnection({
    host : 'localhost',
    user : 'root',
    password : '',
    database : 'passchru',
    multipleStatements : 'true'
});

db.connect((err) =>{
    if(err){
        throw err;
    }
    console.log('MySQL Connected');
});


var dir = path.join(__dirname, 'public');

app.use(express.static(dir));

app.listen('3001', () =>{
    console.log('Server started on port 3001');
});

//Get all food from database
app.get('/food', (req,res)=>{
    db.query('SELECT * FROM food ORDER BY RAND() LIMIT 3', (err, rows, fields)=>{
        if(!err)
            res.send(rows);
        else
        console.log(err);
    })
});


//Get food from database with specific id
app.get('/food/:id', (req,res)=>{
    db.query('SELECT * FROM food WHERE id = ?', [req.params.id], (err, rows, fields)=>{
        if(!err)
            res.send(rows);
        else
        console.log(err);
    })
});

//Get all branches  of specific restaurants from database
app.get('/restau/:id', (req,res)=>{
    db.query('SELECT * FROM restaurant_branches WHERE id = ?', [req.params.id], (err, rows, fields)=>{
        if(!err)
            res.send(rows);
        else
        console.log(err);
    })
});

//Get all sides from specific restaurants from the database
app.get('/restau/:id/sides', (req,res)=>{
    db.query('SELECT * FROM sides WHERE restaurantID = ?', [req.params.id], (err, rows, fields)=>{
        if(!err)
            res.send(rows);
        else
        console.log(err);
    })
});

//Get all drinks from specific restaurants from the database
app.get('/restau/:id/drinks', (req,res)=>{
    db.query('SELECT * FROM drinks WHERE restaurantID = ?', [req.params.id], (err, rows, fields)=>{
        if(!err)
            res.send(rows);
        else
        console.log(err);
    })
});

//INSERT an customer into database
app.post('/customer', (req,res)=>{
    let cust = req.body;
    let sql = `SET @customerFName = ?; SET @customerLName = ?; SET @customerEmail = ?; SET @customerNumber = ?; CALL insertCustomer(@customerFName, @customerLName, @customerEmail, @customerNumber);`;
    db.query(sql, [cust.customerFName, cust.customerLName, cust.customerEmail, cust.customerNumber], (err, result, fields)=>{
        if(!err)
            res.send(result);
        else
        console.log(err);
    })
});

//INSERT a restaurant into database
app.post('/rest', (req,res)=>{
    let rest = req.body;
    let sql = `SET @id = ?; SET @name = ?; CALL insertRest(@name);`;
    db.query(sql, [rest.id, rest.name], (err, result, fields)=>{
        if(!err)
            res.send(result);
        else
        console.log(err);
    })
});

//INSERT a restaurant location into database
app.post('/restbranch', (req,res)=>{
    let restBranch = req.body;
    let sql = `SET @id = ?; SET @restaurant_id = ?; SET @street_address = ?; SET @street_address2 = ?; SET @parish = ?; SET @contact = ?; SET @email = ?; CALL insertRestBranch(@id, @restaurant_id, @street_address, @street_address2, @parish, @contact, @email);`;
    db.query(sql, [restBranch.id, restBranch.restaurant_id, restBranch.street_address, restBranch.street_address2, restBranch.parish, restBranch.contact, restBranch.email], (err, result, fields)=>{
        if(!err)
            res.send(result);
        else
        console.log(err);
    })
});


//INSERT a food into a respective restaurant
app.post('/newfood', (req,res)=>{
    let newfood = req.body;
    let sql = `SET @id = ?; SET @restaurant_id = ?; SET @title = ?; SET @description = ?; SET @price = ?; SET @saves = ?; SET @orders = ?; SET @addedDate = ?; SET @imagename = ?; CALL insertFood(@id, @restaurant_id, @title, @description, @price, @saves, @orders, @addedDate, @imagename);`;
    db.query(sql, [newfood.id, newfood.restaurant_id, newfood.title, newfood.description, newfood.price, newfood.saves, newfood.orders, newfood.addedDate, newfood.imagename], (err, result, fields)=>{
        if(!err)
        //get the order ID
            res.send(result);
        else
        console.log(err);
    })
});

//INSERT items belonging to an order into database
app.post('/orderitems', (req,res)=>{
    let oItems = req.body;
    let sql = `SET @id = ?; SET @orderid = ?; SET @food_id = ?; SET @quantity = ?; CALL insertOrderItems(@id, @orderid, @food_id, @quantity);`;
    db.query(sql, [oItems.id, oItems.orderid, oItems.food_id, oItems.quantity], (err, result, fields)=>{
        if(!err)
            res.send(result);
        else
        console.log(err);
    })
});

//INSERT items belonging to an order confirmation into database
app.post('/confirm', (req,res)=>{
    let oConfirm = req.body;
    let sql = `SET @id = ?; SET @orderID = ?; SET @confirmation_number = ?; SET @pin_number = ?; CALL insertOrderConfirmation(@id, @orderID, @confirmation_number, @pin_number);`;
    db.query(sql, [oConfirm.id, oConfirm.orderID, oConfirm.confirmation_number, oConfirm.pin_number], (err, result, fields)=>{
        if(!err)
            res.send(result);
        else
        console.log(err);
    })
});

//INSERT an order
app.post('/orders', (req,res)=>{
    let orders = req.body;
    console.log(orders)
    let pin = Math.floor(100000 + Math.random() * 900000).toString().substring(1,5);
    let conf = Math.floor(100000000 + Math.random() * 999999999);
    let orderTime = Math.floor(orders.orderTime/1000)
    let schedTime = Math.floor(orders.scheduleTime/1000)
    console.log(orderTime)
    console.log(schedTime)
    console.log(pin)
    console.log(conf)
    //let orderId = 51
    let sql = `insert into orders (customerID,restaurant_id,pickup_location,price,orderTime,scheduleTime,orderStatus,paymentMethod,pin,confirmation) values (?,?,?,?,?,?,?,?,?,?)`
    // let sql = `SET @id = ?; SET @customerID = ?; SET @restaurant_id = ?; SET @pickup_location = ?; SET @price = ?; SET @orderTime = ?; SET @scheduleTime = ?; SET @orderStatus = ?; SET @paymentMethod = ?; CALL insertOrders(@id, @customerID, @restaurant_id, @pickup_location, @price, @orderTime, @scheduleTime, @orderStatus, @paymentMethod);`;
    db.query(sql, [orders.customerID, orders.restaurant_id, orders.pickup_location, orders.price, orderTime, schedTime, 'Received', orders.paymentMethod, pin,conf], (err, result, fields)=>{
         if(!err){
         let oId = result.insertId
         console.log()
         orders.items.map((food)=>{
            let sql2 = `insert into orderitems (orderid,food_id,quantity,sideid,sideqty,drinkid,drinkqty) values (?,?,?,?,?,?,?)`
            db.query(sql2, [oId,food.foodid,1,food.side,1,food.drink,1]), (err, result, fields)=>{
                if(err)
                console.log(err)
            }
        })
            res.send({orderId:oId,pin:pin,conf:conf, orderTime:orderTime, schedTime:schedTime});
        }else{
        console.log(err);
    }
    })
});

//INSERT the status of an order
app.post('/status', (req,res)=>{
    let status = req.body;
    let sql = `SET @id = ?; SET @orderId = ?; SET @orderaccepted = ?; SET @orderReceivedTime = ?; SET @orderStartedTime = ?; SET @orderReadyTime = ?; SET @orderPickUpTime = ?; CALL insertOrderStatus(@id, @orderId, @orderaccepted, @orderReceivedTime, @orderStartedTime, @orderReadyTime, @orderPickUpTime);`;
    db.query(sql, [status.id, status.orderId, status.orderaccepted, status.orderReceivedTime, status.orderStartedTime, status.orderReadyTime, status.orderPickUpTime], (err, result, fields)=>{
        if(!err)
            res.send(result);
        else
        console.log(err);
    })
});


//CUSTOMER ID 1 is used for prototype
//Get all food from database
app.get('/getorders', (req,res)=>{
    db.query('SELECT o.*,r.name FROM orders o inner join restaurant r on o.restaurant_id = r.id where customerID=1 ORDER BY o.id DESC', (err, rows, fields)=>{
        if(!err)
            res.send(rows);
        else
        console.log(err);
    })
});

app.get('/loadorders/:id', (req,res)=>{
    db.query('SELECT * FROM orders WHERE restaurant_id = ? ORDER BY id DESC', [req.params.id], (err, rows, fields)=>{
        if(!err)
            res.send(rows);
        else
        console.log(err);
    })
});

app.post('/orderStarted/', (req,res)=>{
    let status = req.body;
    let sql = 'UPDATE orders SET orderstarted = ? where id = ?'
    db.query(sql, [status.time,status.id], (err, result, fields)=>{
        if(!err){
        let sql = 'UPDATE orders SET orderstatus = ? where id = ?'
        db.query(sql, ["Started",status.id]);
            res.send(result);}
        else
        console.log(err);
    })
})

app.post('/orderCompleted/', (req,res)=>{
    let status = req.body;
    let sql = 'UPDATE orders SET ordercompleted = ? where id = ?'
    db.query(sql, [status.time,status.id], (err, result, fields)=>{
        if(!err){
        let sql = 'UPDATE orders SET orderstatus = ? where id = ?'
        db.query(sql, ["Completed",status.id]);
            res.send(result);}
        else
        console.log(err);
    })
})