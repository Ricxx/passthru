-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 24, 2020 at 06:32 AM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `passchru`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertCustomer` (IN `customerFName` VARCHAR(40), IN `customerLName` VARCHAR(40), IN `customerEmail` VARCHAR(40), IN `customerNumber` VARCHAR(40))  INSERT INTO customer (customerFName, customerLName, customerEmail, customerNumber) 
VALUES (@customerFName, @customerLName, @customerEmail, @customerNumber)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertFood` (IN `id` INT(11), IN `restaurant_id` INT(11), IN `title` VARCHAR(50), IN `description` VARCHAR(50), IN `price` FLOAT, IN `saves` INT(11), IN `orders` INT(11), IN `addedDate` VARCHAR(11), IN `imagename` VARCHAR(90))  INSERT INTO `food` (id, restaurant_id, title, description, price, saves, orders, addedDate, imagename)
VALUES (@id, @restaurant_id, @title, @description, @price, @saves, @orders, @addedDate, @imagename)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertOrderConfirmation` (IN `id` INT(11), IN `orderID` INT(11), IN `confirmation_number` INT(11), IN `pin_number` INT(11))  INSERT INTO `orderconfirmation` (id, orderID, confirmation_number, pin_number)
VALUES 	(@id, @orderID, @confirmation_number, @pin_number)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertOrderItems` (IN `id` INT, IN `orderid` INT, IN `food_id` INT, IN `quantity` INT)  INSERT INTO `orderitems` (id, orderid, food_id, quantity)
VALUES (@id, @orderid, @food_id, @quantity)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertOrders` (IN `id` INT(11), IN `customerID` INT(11), IN `restaurant_id` INT(11), IN `pickup_location` INT(11), IN `price` FLOAT, IN `orderTime` VARCHAR(5), IN `scheduleTime` VARCHAR(5), IN `orderStatus` VARCHAR(50), IN `paymentMethod` VARCHAR(50))  INSERT INTO `orders` (id, customerID, restaurant_id, pickup_location, price, orderTime, scheduleTime, orderStatus, paymentMethod)
VALUES (@id, @customerID, @restaurant_id, @pickup_location, @price, @orderTime, @scheduleTime, @orderStatus, @paymentMethod)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertOrderStatus` (IN `id` INT(11), IN `orderId` INT(11), IN `orderaccepted` VARCHAR(20), IN `orderReceivedTime` VARCHAR(5), IN `orderStartedTime` VARCHAR(5), IN `orderReadyTime` VARCHAR(5), IN `orderPickUpTime` VARCHAR(5))  INSERT INTO `orderstatus` (id, orderId, orderaccepted, orderReceivedTime, orderStartedTime, orderReadyTime, orderPickUpTime)
VALUES (@id, @orderId, @orderaccepted, @orderReceivedTime, @orderStartedTime, @orderReadyTime, @orderPickUpTime)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertRest` (IN `name` VARCHAR(20))  INSERT INTO `restaurant` (id, name) VALUES (@id, @name)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertRestBranch` (IN `id` INT(11), IN `restaurant_id` INT(11), IN `street_address` VARCHAR(90), IN `street_address2` VARCHAR(90), IN `parish` VARCHAR(40), IN `contact` VARCHAR(40), IN `email` VARCHAR(50))  INSERT INTO `restaurant_branches` (id, restaurant_id, street_address, street_address2, parish, contact, email)
VALUES (@id, @restaurant_id, @street_address, @street_address2, @parish, @contact, @email)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectBranch` ()  NO SQL
SELECT * FROM `restaurant_branches` WHERE restaurantid = `3`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `selectFood` ()  SELECT * FROM `food`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `Sp_insertCustomer` (IN `customerFName` VARCHAR(40), IN `customerLName` VARCHAR(40), IN `customerEmail` VARCHAR(40), IN `customerNumber` VARCHAR(40))  INSERT INTO customer(customerFName, customerLName, customerEmail, customerNumber) 
VALUES (@customerFName, @customerLName, @customerEmail, @customerNumber)$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customerID` int(11) NOT NULL,
  `customerFName` varchar(40) DEFAULT NULL,
  `customerLName` varchar(40) DEFAULT NULL,
  `customerEmail` varchar(40) DEFAULT NULL,
  `customerNumber` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customerID`, `customerFName`, `customerLName`, `customerEmail`, `customerNumber`) VALUES
(1, 'Jane', 'Doe', 'jdoe@yahoo.com', '8763309212'),
(2, 'Tony', 'McKenzie', 'tmck@gmail.com', '8763032281'),
(3, 'Timara', 'Simpson', 'marasonsimp@hotmail.com', '8767548191'),
(4, 'Abigail', 'Smith', 'smithabi@gmail.com', '8762230298'),
(5, 'Imani', 'Laing', 'laing.imani10@yahoo.com', '8765731019'),
(15, 'Jimmy', 'Dean', 'jimbodean@hotmail.com', '8764042120'),
(26, 'Julie', 'Palm-Rose', 'rosejulie@gmail.com', '8764349921');

-- --------------------------------------------------------

--
-- Table structure for table `drinks`
--

CREATE TABLE `drinks` (
  `id` int(11) DEFAULT NULL,
  `restaurantID` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `price` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drinks`
--

INSERT INTO `drinks` (`id`, `restaurantID`, `name`, `price`) VALUES
(1, 1, 'Grape', 120),
(2, 1, 'Kola Champagne', 120),
(3, 1, 'Tropicana Fruit Punch', 120),
(4, 2, 'Ting', 130),
(5, 2, 'Ginger Beer', 130),
(6, 2, 'Freshhh', 120),
(7, 3, 'Pepsi', 100),
(8, 3, 'Ocean Spray Grape Splash', 100),
(9, 3, 'Country Style June Plum Juice', 130),
(10, 4, 'Minute Maid Pineapple', 120),
(11, 4, 'Squeeze', 95),
(12, 4, 'Bigga Pineapple Soda', 130),
(13, 5, 'Cran-WATA', 120),
(14, 5, 'WATA', 100),
(15, 5, 'Tropical Rhythm', 180);

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

CREATE TABLE `food` (
  `id` int(11) NOT NULL,
  `restaurant_id` int(11) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `saves` int(11) DEFAULT NULL,
  `orders` int(11) DEFAULT NULL,
  `addedDate` varchar(11) DEFAULT NULL,
  `imagename` varchar(90) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`id`, `restaurant_id`, `title`, `description`, `price`, `saves`, `orders`, `addedDate`, `imagename`) VALUES
(1, 12, 'Krispers Combo', 'Spicy deep fried chicken tenders', 650, 60, 33, '31-01-2020', 'krispers.jpg'),
(2, 19, 'Big Deal', '3 Chicken made with the colonel\'s secret receipe', 850, 50, 41, '23-04-2020', 'bigdeal.jpg'),
(3, 12, 'Famous Bowl Combo', 'Mashed Potato with Popcorn Chicken', 410, 100, 68, '28-07-2020', 'famousbowl.jpg'),
(4, 15, 'Chicken Patty', 'Curry chicken baked in a golden crust', 190, 500, 391, '03-01-2020', 'patty.jpg'),
(5, 11, 'Curry Chicken', 'Chicken curried and served with rice and peas', 600, 400, 258, '06-01-2020', 'currychicken.jpg'),
(6, 11, 'Beef Patty', 'Minced beef baked in a golden crust', 170, 1000, 761, '09-02-2020', 'patty.jpg'),
(7, 1, 'Jerk Chicken Sandwich', 'Jerk chicken breast on Italian bread', 500, 124, 89, '17-05-2020', 'jerkchickensandwich.jpg'),
(8, 1, 'Oxtail with Roti', 'Roti served with oxtail', 750, 20, 12, '22-03-2020', 'oxtailroti.jpg'),
(9, 7, 'Fried Chicken with Rice & Peas', 'Tasty fried chicken with coconut rice and peas', 650, 300, 121, '09-05-2020', 'friedchicken.jpg'),
(10, 9, 'Cheese Patty with Bacon', 'Melted cheese beef & bacon baked in a golden crust', 230, 20, 7, '23-07-2020', 'patty.jpg'),
(14, 4, 'Snapper Sandwich', 'Snapper Fish in a patty', 700, 40, 24, '13-09-2019', 'snapperSandwich.jpg'),
(13, 1, 'Saltfish Fritters', 'Saltfish in a batter thats fried', 200, 70, 59, '15-10-2019', 'saltfishfritters.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `orderconfirmation`
--

CREATE TABLE `orderconfirmation` (
  `id` int(11) NOT NULL,
  `orderID` int(11) DEFAULT NULL,
  `confirmation_number` int(11) DEFAULT NULL,
  `pin_number` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderconfirmation`
--

INSERT INTO `orderconfirmation` (`id`, `orderID`, `confirmation_number`, `pin_number`) VALUES
(5, 23, 537209418, 6418);

-- --------------------------------------------------------

--
-- Table structure for table `orderitems`
--

CREATE TABLE `orderitems` (
  `id` int(11) NOT NULL,
  `orderid` int(11) DEFAULT NULL,
  `food_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `sideid` int(11) NOT NULL,
  `drinkid` int(11) NOT NULL,
  `sideqty` int(11) NOT NULL,
  `drinkqty` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderitems`
--

INSERT INTO `orderitems` (`id`, `orderid`, `food_id`, `quantity`, `sideid`, `drinkid`, `sideqty`, `drinkqty`) VALUES
(5, 23, 8, 1, 0, 0, 0, 0),
(6, 33, 7, 1, 0, 0, 1, 1),
(7, 33, 9, 1, 0, 0, 1, 1),
(8, 33, 3, 1, 0, 0, 1, 1),
(9, 34, 7, 1, 0, 0, 1, 1),
(10, 34, 9, 1, 0, 0, 1, 1),
(11, 34, 3, 1, 0, 0, 1, 1),
(12, 35, 7, 1, 0, 0, 1, 1),
(13, 35, 9, 1, 0, 0, 1, 1),
(14, 35, 3, 1, 0, 0, 1, 1),
(15, 36, 7, 1, 0, 0, 1, 1),
(16, 36, 9, 1, 0, 0, 1, 1),
(17, 36, 3, 1, 0, 0, 1, 1),
(18, 37, 7, 1, 0, 0, 1, 1),
(19, 37, 9, 1, 0, 0, 1, 1),
(20, 37, 3, 1, 0, 0, 1, 1),
(21, 38, 7, 1, 0, 0, 1, 1),
(22, 38, 9, 1, 0, 0, 1, 1),
(23, 38, 3, 1, 0, 0, 1, 1),
(24, 39, 7, 1, 0, 0, 1, 1),
(25, 39, 9, 1, 0, 0, 1, 1),
(26, 39, 3, 1, 0, 0, 1, 1),
(27, 40, 6, 1, 0, 0, 1, 1),
(28, 40, 2, 1, 0, 0, 1, 1),
(29, 40, 7, 1, 0, 0, 1, 1),
(30, 41, 8, 1, 2, 1, 1, 1),
(31, 42, 2, 1, 1, 2, 1, 1),
(32, 42, 5, 1, 3, 3, 1, 1),
(33, 43, 7, 1, 2, 1, 1, 1),
(34, 44, 7, 1, 1, 2, 1, 1),
(35, 44, 14, 1, 1, 0, 1, 1),
(36, 45, 8, 1, 2, 2, 1, 1),
(37, 45, 13, 1, 1, 1, 1, 1),
(38, 46, 8, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `customerID` int(11) DEFAULT NULL,
  `restaurant_id` int(11) DEFAULT NULL,
  `pickup_location` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `orderTime` int(11) DEFAULT NULL,
  `scheduleTime` int(11) DEFAULT NULL,
  `orderStatus` varchar(50) DEFAULT NULL,
  `paymentMethod` varchar(50) DEFAULT NULL,
  `confirmation` int(11) DEFAULT NULL,
  `pin` int(11) DEFAULT NULL,
  `orderstarted` int(11) NOT NULL DEFAULT '0',
  `ordercompleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customerID`, `restaurant_id`, `pickup_location`, `price`, `orderTime`, `scheduleTime`, `orderStatus`, `paymentMethod`, `confirmation`, `pin`, `orderstarted`, `ordercompleted`) VALUES
(46, 1, 5, 1, 850, 1595570923, 1595571638, 'Completed', 'Credit Card', 809569495, 5047, 1595570936, 1595570949),
(45, 1, 5, 1, 1050, 1595570424, 1595571019, 'Completed', 'Credit Card', 984994062, 8563, 1595570459, 1595570473),
(41, 1, 5, 1, 850, 1595560561, 1595562649, 'Started', 'Credit Card', 971244260, 2202, 1595569017, 0),
(42, 1, 5, 1, 1550, 1595563261, 1595565058, 'Completed', 'Credit Card', 214758481, 1568, 1595568943, 1595568985),
(43, 1, 5, 1, 600, 1595569241, 1595569838, 'Received', 'Credit Card', 250459838, 9536, 0, 0),
(44, 1, 5, 1, 1300, 1595569828, 1595570423, 'Started', 'Credit Card', 883299205, 6402, 1595570443, 1595569862);

-- --------------------------------------------------------

--
-- Table structure for table `orderstatus`
--

CREATE TABLE `orderstatus` (
  `id` int(11) NOT NULL,
  `orderId` int(11) DEFAULT NULL,
  `orderaccepted` varchar(20) DEFAULT NULL,
  `orderReceivedTime` varchar(5) DEFAULT NULL,
  `orderStartedTime` varchar(5) DEFAULT NULL,
  `orderReadyTime` varchar(5) DEFAULT NULL,
  `orderPickUpTime` varchar(5) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderstatus`
--

INSERT INTO `orderstatus` (`id`, `orderId`, `orderaccepted`, `orderReceivedTime`, `orderStartedTime`, `orderReadyTime`, `orderPickUpTime`) VALUES
(5, 23, '5', '12:00', '12:17', '12:20', '12:25');

-- --------------------------------------------------------

--
-- Table structure for table `restaurant`
--

CREATE TABLE `restaurant` (
  `id` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `restaurant`
--

INSERT INTO `restaurant` (`id`, `name`) VALUES
(1, 'KFC'),
(2, 'Tastee'),
(3, 'Mothers'),
(4, 'Fatboy'),
(5, 'Bikkles'),
(6, NULL),
(7, NULL),
(8, NULL),
(9, NULL),
(10, 'Little Tokyo'),
(11, NULL),
(12, NULL),
(13, 'Sizzlin Grill'),
(14, 'Fish Pot');

-- --------------------------------------------------------

--
-- Table structure for table `restaurant_branches`
--

CREATE TABLE `restaurant_branches` (
  `id` int(11) NOT NULL,
  `restaurant_id` int(11) DEFAULT NULL,
  `street_address` varchar(90) DEFAULT NULL,
  `street_address2` varchar(90) DEFAULT NULL,
  `parish` varchar(40) DEFAULT NULL,
  `contact` varchar(40) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `restaurant_branches`
--

INSERT INTO `restaurant_branches` (`id`, `restaurant_id`, `street_address`, `street_address2`, `parish`, `contact`, `email`) VALUES
(0, 12, '159 Old Hope Road', 'Kingston 6', 'St. Andrew', '8769900998', 'contact@kfcjamaica.com'),
(1, 19, '43 Waterfront', 'Kingston', 'Kingston', 'contact@kfcjamaica.com', '8769092167'),
(2, 15, '19 Portmore Mall', 'Portmore', 'St. Catherine', '8769895568', 'contact@tastee.com'),
(2, 11, '86 Old Hope Road', 'Kingston 6', 'St. Andrew', '8769397710', 'contact@tastee.com'),
(1, 6, '159 Old Hope Road', 'Kingston 6', 'St. Andrew', '8769993191', 'contact@kfcjamaica.com'),
(5, 1, '76 Old Hope Road', 'Shop 14, Kingston 6', 'St. Andrew', '8769097314', 'contact@bikkles.com'),
(4, 1, '76 Old Hope Road', 'Shop 12, Kingston 6', 'St. Andrew', '8769098348', 'contact@fatboy.com'),
(3, 7, '13 Portmore Mall\r\n', 'Portmore', 'St. Catherine', '8769330982', 'contact@mothers.com'),
(3, 9, '71 Old Hope Road', 'Shop 22, Kingston 6', 'St. Andrew', '8766329031', 'contact@mothers.com'),
(14, 4, '56 Braeton Parkway', 'Portmore', 'St. Catherine', '8769498204', 'contact@fishpot.com'),
(14, 1, '13 Papine Square', 'Kingston 6', 'St. Andrew', '8769227468', 'contact@fishpot.com');

-- --------------------------------------------------------

--
-- Stand-in structure for view `selectdrinks`
-- (See below for the actual view)
--
CREATE TABLE `selectdrinks` (
`name` varchar(50)
,`price` float
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `selectfood`
-- (See below for the actual view)
--
CREATE TABLE `selectfood` (
`id` int(11)
,`restaurant_id` int(11)
,`title` varchar(50)
,`description` varchar(50)
,`price` float
,`saves` int(11)
,`orders` int(11)
,`addedDate` varchar(11)
,`imagename` varchar(90)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `selectsides`
-- (See below for the actual view)
--
CREATE TABLE `selectsides` (
`name` varchar(50)
,`price` float
);

-- --------------------------------------------------------

--
-- Table structure for table `sides`
--

CREATE TABLE `sides` (
  `id` int(11) DEFAULT NULL,
  `restaurantID` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `price` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sides`
--

INSERT INTO `sides` (`id`, `restaurantID`, `name`, `price`) VALUES
(1, 1, 'Fries', 120),
(2, 1, 'Corn', 170),
(3, 1, 'Cookie', 80),
(4, 2, 'Fries', 100),
(5, 2, 'Cheese bread', 150),
(6, 2, 'Soda', 130),
(7, 3, 'Loaded Fries', 180),
(8, 3, 'Soda', 100),
(9, 3, 'Rice', 140),
(10, 4, 'Sweet Potato Fries', 150),
(11, 4, 'Juice', 130),
(12, 5, 'Roti', 100),
(13, 5, 'Ground Provision', 150),
(14, 5, 'Rice', 150);

-- --------------------------------------------------------

--
-- Structure for view `selectdrinks`
--
DROP TABLE IF EXISTS `selectdrinks`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `selectdrinks`  AS  select `drinks`.`name` AS `name`,`drinks`.`price` AS `price` from `drinks` ;

-- --------------------------------------------------------

--
-- Structure for view `selectfood`
--
DROP TABLE IF EXISTS `selectfood`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `selectfood`  AS  select `food`.`id` AS `id`,`food`.`restaurant_id` AS `restaurant_id`,`food`.`title` AS `title`,`food`.`description` AS `description`,`food`.`price` AS `price`,`food`.`saves` AS `saves`,`food`.`orders` AS `orders`,`food`.`addedDate` AS `addedDate`,`food`.`imagename` AS `imagename` from `food` ;

-- --------------------------------------------------------

--
-- Structure for view `selectsides`
--
DROP TABLE IF EXISTS `selectsides`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `selectsides`  AS  select `sides`.`name` AS `name`,`sides`.`price` AS `price` from `sides` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customerID`);

--
-- Indexes for table `food`
--
ALTER TABLE `food`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderconfirmation`
--
ALTER TABLE `orderconfirmation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderstatus`
--
ALTER TABLE `orderstatus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `restaurant`
--
ALTER TABLE `restaurant`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customerID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `food`
--
ALTER TABLE `food`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `orderconfirmation`
--
ALTER TABLE `orderconfirmation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `orderitems`
--
ALTER TABLE `orderitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `orderstatus`
--
ALTER TABLE `orderstatus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `restaurant`
--
ALTER TABLE `restaurant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
