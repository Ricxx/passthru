import React, { useEffect, useState } from 'react';
import { View, StyleSheet, StatusBar, } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { CartContext } from './context/CartContext';
import home from './screens/Home'
import Product from './screens/Product'
import Checkout from './screens/Checkout';
import OrderComplete from './screens/OrderComplete';
import Orders from './screens/Orders';
import OrderDetails from './screens/OrderDetails';
import AsyncStorage from '@react-native-community/async-storage';
import Base from './screens/Base'

const homeStack = createStackNavigator();

const App = () => {
  //Text.defaultProps.style = {fontFamily: Roboto}
  const cartItemsContext = () => {
    const [ item, setItem ] = useState();

//{id:1,name:'burger',amount:0,cost:90},{id:1,name:'burger',amount:0,cost:90}
    async function clearCartFile(){
      try {
        await AsyncStorage.removeItem('userCart')
        console.log('cleared')
      } catch(e) {
        console.log('Failed to save cart.')
      }
    }

    async function getCart(){
      try {
        const jsonValue = await AsyncStorage.getItem('userCart')
        return jsonValue != null ? setItem(JSON.parse(jsonValue)) : null//setItem('')//null
      } catch(e) {
        console.log('Failed to load cart.',e)
      }
    }

    async function saveCart(){
      try {
        await AsyncStorage.setItem('userCart', JSON.stringify(item))
      } catch(e) {
        console.log('Failed to save cart. ', e)
      }
    }

    useEffect(() => {
      //clearCartFile()
      getCart();
    }, [])

    useEffect(() => {
      if(item!=null)
        saveCart();
    }, [item])

    const cartFunctions = () => {
      return {
        addToCart: async(obj) => {
          let x = await getCart();
          console.log(`x is :::::${item}::::: ${x}`);
          (item === undefined || item === "" || x===null ) ? setItem([obj]) :
            setItem([...item,obj])
            saveCart()
            console.log(item)
        },
        updateCart: (itemID, qty = 0) => {
          console.log(`${itemID} - ${qty}`)
        },
        deleteFromCart: (itemID) => {
          console.log(`${itemID}`)
        },
        clearCart: async() => {
          await clearCartFile();
          setItem(null);
          console.log('cart cleared')
        }
      }
    }
    return ({ item, setItem, cartFunctions });
  }

  return (
    <NavigationContainer>
        <CartContext.Provider value={cartItemsContext}>
        <StatusBar hidden/>
          <homeStack.Navigator screenOptions={{
            headerShown: false
          }}>
            <homeStack.Screen name="Home" component={home} />
            <homeStack.Screen name="Product" component={Product} />
            <homeStack.Screen name="Orders" component={Orders} />
            <homeStack.Screen name="OrderDetails" component={OrderDetails} />
            <homeStack.Screen name="Checkout" component={Checkout} />
            <homeStack.Screen name="Complete" component={OrderComplete} />
          </homeStack.Navigator>
        </CartContext.Provider>
      </NavigationContainer>
  );
};

const styles = StyleSheet.create({

});

export default App;
