import { Dimensions } from 'react-native';

const DEFAULT_WIDTH = 480;
const DEFAULT_HEIGHT = 853;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const normalizeW = (W) => {
    let wRatio = windowWidth/DEFAULT_WIDTH;
    return Math.round(W*wRatio)
}
export const normalizeH = (H) => {
    let hRatio = windowHeight/DEFAULT_HEIGHT;
    return Math.round(H*hRatio)
}
