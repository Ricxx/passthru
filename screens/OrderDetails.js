import React, {useContext,useState, useEffect} from 'react'
import { View, Text, ScrollView, Image } from 'react-native'
import Base from './Base'
import { normalizeH, normalizeW } from '../functions/global'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/Feather'
import ChargeItem from '../components/ChargeItem'
import { CartContext } from '../context/CartContext'
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment'

export default function OrderDetails(props) {
    //load order by order details
    const {orderId ,restaurant, total, pin, conf, orderTime, orderStarted, orderCompleted} = props.route.params
    const [showPin, setShowPin] = useState(false)
    console.log(props.route.params)
    
    //Get order status and times, if time set, change box update time.
    
    // const cartItems = useContext(CartContext)();
    const [chkout, setChkout] = useState({ready:false,data:''})
    // console.log('Cart is::', cartItems.item)
    // async function getCart(){
    //     try {
    //       const jsonValue = await AsyncStorage.getItem('userCart')
    //       return jsonValue != null ? setChkout({ready:true,data:JSON.parse(jsonValue)}) : null//setItem('')//null
    //     } catch(e) {
    //       console.log('Failed to load cart.',e)
    //     }
    //   }
    // useEffect(() => {
    //     getCart()  
    // }, [])

    console.log('OrderTime::::: ',orderTime)
    function confirmationNumber(num){
        return num.toString().replace(/(\d{3})+(\d{3})+(\d{3})/g, "$1-$2-$3")
    }

    return (
        <Base>
            <View style={{ marginTop: normalizeH(-50), marginBottom: normalizeH(20), alignItems: 'center', alignSelf: 'center', width: '90%' }}>
                <Text style={{ fontSize: normalizeW(36), fontFamily: 'RobotoLight' }}>Order #{orderId}</Text>
            </View>
            {/* For each restaurant, show schedule time, address, confirmation & pin, cancel/report, save order */}
            <ScrollView contentContainerStyle={{ alignItems: 'center' }}>
                <View>
                    {/* extract to a component and just render the components */}
                    <Text style={{fontSize:20,fontFamily:'RobotoLight'}}>
                        {props.route.params.restaurant}
                    </Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                        <Text style={{fontSize:16,fontFamily:'Roboto'}}>
                            Confirmation#: {confirmationNumber(conf)}
                    </Text>
                        <View style={{marginBottom:10, justifyContent:'center',alignItems:'center'}}>
                            <Text style={{fontSize:16,fontFamily:'Roboto',marginBottom:5}}>
                                Pin#: {showPin? pin : "****"}
                    </Text>
                    <TouchableOpacity activeOpacity={0.85} onPress={()=>setShowPin(!showPin)}>
                    <View style={{backgroundColor:'#000',borderRadius:4}}>
                                <Text style={{padding:5,fontSize:14,color:'#fff'}}><Icon name="eye" size={normalizeW(14)} color='#fff' /> {showPin ? "Hide" : "Reveal"} Pin</Text>
                            </View>
                    </TouchableOpacity>
                            
                        </View>
                    </View>
                    <Text  style={{marginBottom:20,color:'#333',fontSize:14}}>
                        Tap the icon to see the pin, use this number to collect your order!
                    </Text>
                </View>
                
                            
                <View style={{ width: '80%', marginTop: normalizeH(20) }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Icon color='#E9687E' size={normalizeW(32)} name="check-square" style={{ marginLeft: normalizeW(-12) }} />
                        <View style={{ justifyContent: 'center' }}>
                            <Text style={{ fontSize: normalizeW(32), marginLeft: '10%', textAlign: 'center', color: '#333333', fontFamily: 'Roboto' }}>Order Received</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 2 }}>
                        <Image source={require('../assets/images/verticalDivider.png')} style={{}} />
                        <Text style={{ alignSelf: 'center', fontSize: normalizeW(22), fontFamily: 'RobotoLight' }}>@{moment(orderTime*1000).format('LT')}</Text>
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Icon color={orderStarted > 0 ? '#E9687E':'#666'} size={normalizeW(32)} name={orderStarted > 0 ? "check-square" : "square"} style={{ marginLeft: normalizeW(-12) }} />
                        <View style={{ justifyContent: 'center' }}>
                            <Text style={{ fontSize: normalizeW(32), marginLeft: '10%', textAlign: 'center', color: '#333333', fontFamily: 'Roboto' }}>Order Started</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 2 }}>
                        <Image source={require('../assets/images/verticalDivider.png')} style={{}} />
                        <Text style={{ alignSelf: 'center', fontSize: normalizeW(22), fontFamily: 'RobotoLight' }}>{orderStarted> 0 ? '@' + moment(orderStarted*1000).format('LT') : "Waiting..."}</Text>
                    </View>
                    
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Icon color={orderCompleted > 0 ? '#E9687E':'#666'} size={normalizeW(32)} name={orderCompleted > 0 ? "check-square" : "square"} style={{ marginLeft: normalizeW(-12) }} />
                        <View style={{ justifyContent: 'center' }}>
                            <Text style={{ fontSize: normalizeW(32), marginLeft: '10%', textAlign: 'center', color: '#333333', fontFamily: 'Roboto' }}>Order Ready</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 2 }}>
                        <Image source={require('../assets/images/WverticalDivider.png')} style={{}} />
                        <Text style={{ alignSelf: 'center', fontSize: normalizeW(22), fontFamily: 'RobotoLight' }}>{orderCompleted> 0 ? '@' + moment(orderCompleted*1000).format('LT') : "Waiting..."}</Text>
                    </View>
                </View>

                <View style={{marginTop:40, backgroundColor: '#FE5959', width: '40%', borderRadius: 10, padding: 3, zIndex: 3 }}>
                            <Text style={{ color: '#FFF', fontSize: normalizeW(28), textAlign: 'center', fontFamily: 'Roboto' }}>Summary</Text>
                        </View>
                        <View style={{ backgroundColor: '#FEF9E3', width: '90%', height: normalizeH(450), padding: 10, marginTop: normalizeH(-20), zIndex: 0 }}>
                            <View style={{ flex: 4, }}>
                                <ScrollView style={{ width: '100%', marginTop: normalizeH(30), paddingRight: 7 }}>
                                {
                                        chkout.ready && chkout.data.map((item,idx)=>{
                                            return(
                                                <ChargeItem key={idx} drink={item.drink} drinkqty={item.drinkqty} 
                                                side={item.side} sideqty={item.sideqty} restaurantname={item.restaurantname} 
                                                price={item.price}  title={item.title}
                                                />
                                            )
                                        })
                                    }
                                    {/* <ChargeItem key={'fee'} title={'Service Fee'} price={100} restaurantname={'Passchru'} /> */}
                                </ScrollView>
                            </View>
                            </View>
                <View style={{flex:1,width:'100%'}}>

                    <View style={{backgroundColor:'#ffffca',height:normalizeH(100), marginBottom:normalizeH(20)}}>
                        <View style={{backgroundColor:'#ffffca',padding:5,borderRadius:20,justifyContent:'center',alignItems:'center',flexDirection:'row',marginBottom:10 }}>
                            <Icon name="bell" size={normalizeW(24)} color='#333' style={{marginRight:normalizeW(5)}}/>
                            <Text style={{color:'#333', fontSize:20}}>Notices</Text>
                        </View>
                        <Text style={{marginLeft:10}}> [9:40AM] Your order has been accepted by K. Bell</Text>
                    </View>

                    <View style={{flexDirection:'row', justifyContent:'space-between',width:'100%'}}>
                    
                    <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                    <TouchableOpacity>
                        <View style={{backgroundColor:'#fa0',padding:5,borderRadius:7,justifyContent:'center',alignItems:'center',flexDirection:'row',width:'100%' }}>
                            <Icon name="alert-triangle" size={normalizeW(32)} color='#fff' style={{marginRight:normalizeW(20)}}/>
                            <Text style={{color:'#fff', fontSize:20}}>Raise Dispute</Text>
                        </View>
                    </TouchableOpacity>
                    </View>

                    <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                    <TouchableOpacity>
                        <View style={{backgroundColor:'#f00',padding:5,borderRadius:7,justifyContent:'center',alignItems:'center',flexDirection:'row',width:'100%' }}>
                            <Icon name="frown" size={normalizeW(32)} color='#fff' style={{marginRight:normalizeW(20)}}/>
                            <Text style={{color:'#fff', fontSize:20}}>Cancel Order</Text>
                        </View>
                    </TouchableOpacity>
                    </View>

                    </View>
                </View>

                {/* <View style={{ width: '40%' }}>
                    <TouchableOpacity>
                        <View style={{ padding: 10, backgroundColor: '#000', borderRadius: 10, justifyContent: 'center' }}>
                            <Text style={{ color: '#fff', justifyContent: 'center', fontSize: normalizeW(22) }}>
                                <Icon name="chevron-left" size={22} color="#fff" />
                                Back to Orders
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View> */}
            </ScrollView>
        </Base>
    )
}
