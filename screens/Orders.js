import React, { useEffect, useState } from 'react'
import { View, Text, ScrollView } from 'react-native'
import { normalizeW, normalizeH } from '../functions/global'
import Base from '../screens/Base'
import HistoryItem from '../components/HistoryItem'

export default function Orders(props) {
    const [orders, setOrders] = useState({ready:false,data:{}})
    // Load all orders for customer id xyz
    useEffect(() => {
        const getOrders = async() =>{
            let orderList = await fetch('http://192.168.0.12:3001/getorders');
            orderList = await orderList.json()
            setOrders({ready:true,data:orderList})
        } 
        getOrders();
        
    }, [])
    const navToOrder = (oId,rName,oTotal,pin,conf,orderTime,orderStarted,orderCompleted) =>{
        props.navigation.navigate('OrderDetails',{orderId:oId,restaurant:rName,total:oTotal,pin,conf,orderTime,orderStarted,orderCompleted})
    }
    console.log("Orders:::::",orders)
    return (
        <Base>
            <View style={{marginTop:normalizeH(-50),marginBottom:normalizeH(20),alignItems:'center',alignSelf:'center',width:'90%'}}>
                <Text style={{fontSize:normalizeW(36),fontFamily:'RobotoLight'}}>Order History</Text>
            </View>
            <View style={{marginTop:normalizeH(20),alignItems:'center',alignSelf:'center',width:'95%'}}>
                <ScrollView style={{paddingRight:5,overflow:'scroll'}}>
                {
                    orders.ready && orders.data.length>0 && orders.data.map((item,idx)=>{
                                            return(
                                        <HistoryItem key={idx} orderId={item.id} 
                                        orderStatus={item.orderStatus} restaurant={item.name} 
                                        total={item.price} orderTime={item.orderTime} nav={navToOrder} 
                                        conf={item.confirmation} pin={item.pin} 
                                        orderStarted={item.orderstarted} orderCompleted={item.ordercompleted}/>
                                            )
                                        })
                }

                    {/* <HistoryItem/>
                    <HistoryItem/>
                    <HistoryItem/>
                    <HistoryItem/>
                    <HistoryItem/>
                    <HistoryItem/>
                    <HistoryItem/> */}
                </ScrollView>
            </View>
        </Base>
    )
}
