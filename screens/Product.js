import React, {useState, useContext, useEffect} from 'react'
import { View, Text, Image,ScrollView } from 'react-native'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button'
//import Base from './Base'
import Header from '../components/HeaderComponent'
import Footer from '../components/FooterNav'
import { CartContext } from '../context/CartContext'
import { normalizeH, normalizeW } from '../functions/global'
import { TouchableOpacity } from 'react-native-gesture-handler'
import {Picker} from '@react-native-community/picker';


export default function Product(props) {
    const cartItems = useContext(CartContext)();
    //Simulate data seek based on props.navigation params
    const [ food, setFood ] = useState({ready:false,data:''})
    const [ options, setOptions ] = useState({sides:0,drinks:0,branch:''});
    const [ type, setType ] = useState('sides');
    let productID = props.route.params.itemId
    //console.log(productID);
    //console.log('foodItem:', food.data.imagename);
    
    useEffect(() => {
        async function loadData(){
            let data1 = await fetch('http://192.168.0.12:3001/food/'+productID)
            data1 = await data1.json()
            //console.log(data1)
            setTimeout(() => {
                setFood({ready:true,data:data1[0]})
            }, 500);
            
        }loadData()
    }, [])

    const data = {
        restaurant: 'Fromage',
        title: 'Bistro Burger',
        price: 850,
        image: 'doThisLater',
        desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Bibendum aliquam ipsum eu etiam dolor et. Amet nec elementum quam a. Praesent erat lobortis rhoncus non purus.',
    }
    var sideOptions = [
        {label: 'No Sides', value: 0 },
        {label: 'Fries', value: 1 },
        {label: 'Wedges', value: 2 },
        {label: 'Onion Rings', value: 3 },
      ];
    var drinkOptions = [
        {label: 'No Drink', value: 0 },
        {label: 'Soda', value: 1 },
        {label: 'Water', value: 2 },
        {label: 'Orange Juice', value: 3 },
      ];
    var branches =[
        {id: 1, name: 'Bistro - 8 Hillcrest Ave, Kgn'},
        {id: 2, name: 'Brasserie - 67 Constant Spring Rd, Kgn'},
    ]

    // image, name, id, price, sides & drink, restaurant name & id
    const addToCart = () => {
        var result1 = drinkOptions.find(obj => {
            return obj.value === options.drinks
          })
          var result = sideOptions.find(obj => {
            return obj.value === options.sides
          })
          console.log(result.label)
        let obj = {foodid:food.data.id,
            title:food.data.title,
            price:food.data.price,
            image:food.data.imagename,
            restaurantid:food.data.restaurant_id,
            restaurantname:'Fromage',
            branch:options.branch,
            side:options.sides,sidename:result.label,sideqty:1,
            drink:options.drinks,drinksname:result1.label,drinkqty:1}
        cartItems.cartFunctions().addToCart(obj);
        alert('Item added!');
        //console.log(cartItems.item)
        //props.navigation.navigate('Checkout')
    }
    if(!food.ready){
    return(
        <View style={{flex:1, backgroundColor:'#fff',justifyContent:'center',alignItems:'center'}}>
            <Image source={require('../assets/images/loading.gif')} style={{height: 400, width: 400}}
            resizeMode='contain' />
            <Text style={{fontSize:48,fontFamily:'RobotoThin'}}>Loading...</Text>
        </View>

    )
    }
    return (
        <View style={{ flex: 1 }}>
            <View style={{ flex: 3, zIndex: 2, alignSelf: 'flex-start', marginTop: normalizeH(20) }}>
                <Header />
            </View>
            <View style={{ position: 'absolute', top: 0, left: 0 }}>
                <Image source={{uri:'http://192.168.0.12:3001/'+food.data.imagename}}
                    style={{ position: 'absolute', resizeMode: 'cover', width: normalizeW(500), height:normalizeW(500), borderBottomRightRadius: 0 }}
                />
                {/* <Image source={require('../assets/food/BistroBurger.png')}
                    style={{ position: 'absolute', resizeMode: 'cover', width: normalizeW(500), borderBottomRightRadius: 0 }}
                /> */}
            </View>
            <View style={{ flex: 7, backgroundColor: 'transparent' }}>
                <Image source={require('../assets/images/BottomHalf.png')} style={{ position: 'absolute', top: 0, width: normalizeW(500), resizeMode: 'stretch', }} />

                {/* Top of second half */}
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View style={{ flex: 1, }}>
                        <TouchableOpacity activeOpacity={0.90}>
                            <Image source={require('../assets/images/addCircle.png')} style={{width:normalizeW(64),height:normalizeH(64), marginTop: normalizeH(90), marginLeft: normalizeW(45) }} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, alignItems: 'flex-end', zIndex: 2, marginTop: normalizeH(40), marginRight: normalizeW(20) }}>
                        <Text style={{ fontSize: normalizeW(32), marginTop: normalizeH(30), fontFamily: 'RobotoLight' }}>${food.data.price}.00</Text>
                    </View>
                </View>
                <ScrollView style={{width:'90%', marginLeft:'auto',marginBottom:normalizeH(100),marginTop:normalizeH(20)}} contentContainerStyle={{justifyContent:'center'}}>
                    <View style={{}}>
                        <Text style={{fontFamily:'RobotoLight',fontSize:normalizeW(18), color:'#555'}}>{data.restaurant}</Text>
                        <Text style={{fontFamily:'RobotoThin',fontSize:normalizeW(36)}}>{food.data.title}</Text>
                        <Text style={{fontFamily:'Roboto',fontSize:normalizeW(15),color:'#555'}}>{food.data.description}</Text>
                        <View style={{backgroundColor:'#FFFFDD',width:'90%',borderRadius:20,padding:20,marginVertical:normalizeH(20)}}>
                            <View> 
                            <Text style={{fontFamily:'RobotoLight',fontSize:normalizeW(16)}}>Select pickup location: </Text>   
                            <Picker
                            selectedValue={options.branch}
                            // style={{height: 50, width: 100}}
                            onValueChange={(itemValue, itemIndex) =>
                                setOptions({...options,branch: itemValue})
                            }>
                            {
                                branches.map((branch,idx)=>{
                                    return (
                                        <Picker.Item key={idx} label={branch.name} value={branch.id} />
                                );
                                })
                            }
                            {/* <Picker.Item label="Bistro - 8 Hillcrest Ave, Kgn" value="8 Hillcrest Ave" />
                            <Picker.Item label="Brasserie - 67 Constant Spring Rd, Kgn" value="67 Constant Spring Rd" /> */}
                            </Picker>
                            </View>
                            {/* <Text style={{fontFamily:'RobotoLight',fontSize:16}}>Address: 8 Hillcrest Ave, Kingston</Text> */}
                        </View>
                    </View>
                    <View style={{flexDirection:'row', justifyContent:'space-evenly',marginTop:normalizeH(20)}}>
                        <TouchableOpacity activeOpacity={0.75} onPress={()=>setType('sides')}>
                            <Text style={{fontSize:normalizeW(22), color:type=='sides'?'#492800':'#000',}}> Sides</Text>
                        </TouchableOpacity>
                        <TouchableOpacity activeOpacity={0.75} onPress={()=>setType('drinks')}>
                            <Text style={{fontSize:normalizeW(22), color:type=='drinks'?'#492800':'#000'}}> Drinks</Text>
                        </TouchableOpacity>
                        {/* <TouchableOpacity activeOpacity={0.75}>
                            <Text style={{fontSize:22}}> Options</Text>
                        </TouchableOpacity> */}
                    </View>
                    <View style={{backgroundColor:'#FEF9F3', width:'90%',padding:10,marginTop:normalizeH(10),borderRadius:20}}>
                        
                           { type=='sides'&&
                        (
                        <View style={{marginTop:normalizeH(15)}}>
                        <RadioForm
                            radio_props={sideOptions}
                            initial={options.sides}
                            buttonColor={'#E9687E'}
                            selectedButtonColor={'#E9687E'}
                            onPress={(value) => {setOptions({...options,sides:value})}}
                            /> 
                        </View>
                        )}
                        {type=='drinks'&&(
                        <View style={{marginTop:normalizeH(15)}}>
                        <RadioForm
                            radio_props={drinkOptions}
                            initial={options.drinks}
                            buttonColor={'#E9687E'}
                            selectedButtonColor={'#E9687E'}
                            onPress={(value) => {setOptions({...options,drinks:value})}}
                            /> 
                        </View>
                        )
                        }
                        
                        <View style={{backgroundColor:'#000', position:'absolute',width:'40%',bottom:0, right:0,borderBottomEndRadius:20,borderTopLeftRadius:40}}>
                        <TouchableOpacity onPress={()=>addToCart()}>
                        <View style={{flex:1,padding:7}}>
                            <Text style={{fontFamily:'RobotoLight',fontSize:normalizeW(22),color:'#fff',textAlign:'center'}}>Add to Cart</Text>
                        </View>
                            </TouchableOpacity>
                            </View>
                    </View>
                        
                       
                
                </ScrollView>
            </View>

            <View style={{ zIndex: 3, position: 'absolute', bottom: 0, width: '100%', height: normalizeH(80) }}>
                <Footer />
            </View>
        </View>
    )
}
