import React from 'react'
import { View, Text, Image } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import { normalizeW, normalizeH } from '../functions/global'
import FooterNav from '../components/FooterNav'
import ChargeItem from '../components/ChargeItem'
import Icon from 'react-native-vector-icons/Entypo'
import { CartContext } from '../context/CartContext'
import AsyncStorage from '@react-native-community/async-storage';

export default function OrderComplete(props) {
    // const [ chkout, setChkout ] = React.useState({ready:false,data:''});
    let num = props.route.params.confirmation
    // async function getCart(){
    //     try {
    //       const jsonValue = await AsyncStorage.getItem('userCart')
    //       return jsonValue != null ? setChkout({ready:true,data:JSON.parse(jsonValue)}) : null//setItem('')//null
    //     } catch(e) {
    //       console.log('Failed to load cart.',e)
    //     }
    //   }
    //   React.useEffect(() => {
    //     getCart()
    //     //console.log(chkout)
    // }, [])

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    function confirmationNumber(){
        console.log("Inside Function: ",num)
        return num.toString().replace(/(\d{3})+(\d{3})+(\d{3})/g, "$1 - $2 - $3")
    }

    console.log(props.route.params)
    return (
        <View style={{backgroundColor:'#fff',flex:1}}>
        <View style={{flex:8}}>
        <ScrollView style={{overflow:'visible',marginBottom:normalizeH(80)}} contentContainerStyle={{width:'100%',marginTop:normalizeH(20)}}>
            <View style={{alignItems:'center'}}>
            <Image source={require('../assets/images/ThankYou.png')}/>
            <Text style={{fontFamily:'AlluraRegular',fontSize:normalizeW(100),color:'#FE5959'}}>Thank You</Text>
            <Text style={{fontFamily:'RobotoLight',fontSize:normalizeW(18),marginTop:normalizeH(20),color:'#636363'}}>We have received <Text style={{color:'#FE5959'}}>your order.</Text></Text>
            </View>
            <View style={{alignItems:'center',marginTop:normalizeH(50)}}>
                <Text style={{fontFamily:'Roboto',letterSpacing:1.5,fontSize:normalizeW(32),color:'#3D3D3D'}}>CONFIRMATION #</Text>
                <Image source={require('../assets/images/barCodeImg.png')} style={{marginTop:20}}/>
                <Text style={{fontFamily:'Roboto',letterSpacing:1,fontSize:normalizeW(28),color:'#3D3D3D'}}>{confirmationNumber()}</Text>
            </View>

            {/* <View style={{marginTop:normalizeH(50),alignItems:'center'}}>
            <View style={{ backgroundColor: '#FE5959', width: '40%', borderRadius: 10, padding: 3, zIndex: 3 }}>
                            <Text style={{ color: '#FFF', fontSize: normalizeW(28), textAlign: 'center', fontFamily: 'Roboto' }}>Summary</Text>
                        </View>
                        <View style={{ backgroundColor: '#FEF9E3', width: '90%', height: normalizeH(450), padding: 10, marginTop: normalizeH(-20), zIndex: 0 }}>
                            <View style={{ flex: 4, }}>
                                <ScrollView style={{ width: '100%', marginTop: normalizeH(30), paddingRight: 7 }}>
                                {
                                        chkout.ready && chkout.data.length>0 && chkout.data.map((item,idx)=>{
                                            return(
                                                <ChargeItem key={idx} drink={item.drink} drinkqty={item.drinkqty} 
                                                side={item.side} sideqty={item.sideqty} restaurantname={item.restaurantname} 
                                                price={item.price}  title={item.title}
                                                />
                                            )
                                        })
                                    }
                                    <ChargeItem key={'fee'} title={'Service Fee'} price={100} restaurantname={'Passchru'} />
                                </ScrollView>
                            </View>
                            <View style={{ flex: 1, }}>
                                <Image source={require('../assets/images/checkoutDivider.png')} style={{ marginVertical: normalizeH(10) }} />
                                <View style={{ marginLeft: normalizeW(20), flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={{ fontSize: normalizeW(32), fontFamily: 'RobotoLight' }}>Total</Text>
                                    <Image source={require('../assets/images/totalDot.png')} style={{ marginHorizontal: 10, width: 15, height: 15 }} />
                                    <Text style={{ fontSize: normalizeW(32), fontFamily: 'RobotoLight' }}>${numberWithCommas(props.route.params.total)}.00</Text>
                                </View>
                            </View>
                        </View> */}
                        
                        <View style={{width:'100%',alignSelf:'center',flexDirection: 'row', alignItems: 'center',justifyContent:'center'}}>
                        <TouchableOpacity activeOpacity={.85} onPress={()=>props.navigation.navigate('Orders')} style={{marginTop:normalizeH(20),alignItems:'center'}}>

                        <View style={{backgroundColor:'#000',paddingHorizontal:normalizeW(20),paddingVertical:normalizeH(8),borderRadius:10,alignItems:'center'}}>
                        <Text style={{fontFamily:'RobotoLight',fontSize:normalizeW(24),color:'#fff'}}>View Your Orders <Icon name="chevron-thin-right" size={22} color="#fff"/></Text>
                        </View>
                        </TouchableOpacity>
            
            </View>
        </ScrollView>
        </View>
        <View>
            
        </View>
        {/* <FooterNav/> */}
        </View>
    )
}
