import React from 'react'
import { View, ScrollView } from 'react-native'
import Header from '../components/HeaderComponent'
import FooterNav from '../components/FooterNav'
import { normalizeH } from '../functions/global'
import { setCustomText } from 'react-native-global-props';

const customTextProps = { 
  style: { 
    fontFamily: "Roboto"
  }
}

function Base({ children }) {
    setCustomText(customTextProps);
    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>

            <View style={{ flex: 1, marginTop: normalizeH(20), backgroundColor: '#fff' }}>
                <Header />
            </View>

            <View style={{ flex: 8,marginTop:normalizeH(20), backgroundColor: '#fff' }}>
                <View style={{paddingBottom:30, backgroundColor: '#fff'}}>
                  {children}
                </View>
            </View>

            <View style={{ flex: 1 }}>
                <FooterNav />
            </View>

        </View>
    )
}

export default React.memo(Base);
