import React, { useContext } from 'react'
import { View, Text, Image, ScrollView } from 'react-native'
import { CartContext } from '../context/CartContext'
import {normalizeW, normalizeH} from '../functions/global'
import Base from './Base'
import ProductRoll from '../components/ProductRoll'


export default function Home(props) {
    const cartItems = useContext(CartContext)();
    console.log(cartItems.item)



    return (
        <Base>

        {/* Tabs */}
            <View style={{flexDirection:'row',justifyContent:'space-around',alignItems:'center'}}>
                <View >
                
                    <Text style={{color:'#FFC1B4', fontSize:normalizeW(22), fontFamily:'Roboto'}}> 
                    <Image source={require('../assets/images/activeDot.png')} 
                    style={{width:5,height:5}}/>  
                     Discover
                    </Text>
                </View>
                <View>
                    <Text style={{color:'#000', fontSize:normalizeW(22), fontFamily:'Roboto'}}> Restaurants
                    </Text>
                </View>
                <View>
                    <Text style={{color:'#000', fontSize:normalizeW(22), fontFamily:'Roboto'}}> Categories
                    </Text>
                </View>
            </View>

            {/* product rolls */}
            <ScrollView style={{marginTop:normalizeH(20),}} contentContainerStyle={{overflow:'visible'}}>
            <ProductRoll type={'Best-Sellers'} limit={5} ico={'bseller'}/>
            <ProductRoll type={'Trending'} limit={5} ico={'trend'}/>
            <ProductRoll type={'New Items'} limit={5} ico={'newItems'}/>
            </ScrollView>
            
        </Base>
    )
}
