import React, { useContext, useState, useEffect } from 'react'
import { View, Text, ScrollView, Image } from 'react-native'
import Base from './Base'
import { normalizeW, normalizeH } from '../functions/global'
import ChargeItem from '../components/ChargeItem'
import PaymentOptions from '../components/PaymentOptions'
import PickUp from '../components/PickUp'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { CartContext } from '../context/CartContext'
import AsyncStorage from '@react-native-community/async-storage';

export default function Checkout(props) {
    const cartItems = useContext(CartContext)();
    const [ chkout, setChkout ] = useState({ready:false,data:''});
    const [ pickup, setPickup ] = useState(0);
    const [ payment, setPayment ] = useState(0);
    let sum = 0;

    async function getCart(){
        try {
          const jsonValue = await AsyncStorage.getItem('userCart')
          return jsonValue != null ? setChkout({ready:true,data:JSON.parse(jsonValue)}) : null//setItem('')//null
        } catch(e) {
          console.log('Failed to load cart.',e)
        }
      }

    useEffect(() => {
        getCart()
        console.log(chkout)
    }, [])

    if(chkout.data.length>0){
        sum = chkout.data.reduce((a, {price}) => a + price, 0);
    }
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    const submitOrder = async() =>{
        //add info to DB, check db for item ids & price? NOT IN PROTOTYPE.
        const orderData = {customerID:1,restaurant_id:5,pickup_location:1,price:sum+100,orderTime:Date.now(),scheduleTime:pickup,paymentMethod:"Credit Card",items:chkout.data}
        console.log("OrderData: ", orderData )
        //Add to db and submit, db should return confirmation# and PIN
        let rawData = await fetch('http://192.168.0.12:3001/orders',{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify(orderData)})
            rawData = await rawData.json();
            console.log("The raw data is:", rawData)
        //Clear cart as well
        cartItems.cartFunctions().clearCart();
        props.navigation.navigate('Complete',{total: sum+100,time:rawData.orderTime,schedTime:rawData.schedTime,payment:payment,orderNumber:rawData.orderId,confirmation:rawData.conf,pin:rawData.pin})
    }
    return (
        <Base>
            <View>
                <View style={{ alignItems: 'center' }}>
                    <ScrollView style={{width:'100%', overflow:'scroll' }} contentContainerStyle={{alignItems: 'center'}}>
                        <View style={{ backgroundColor: '#000', width: '40%', borderRadius: 10, padding: 3, zIndex: 3 }}>
                            <Text style={{ color: '#FFF', fontSize: normalizeW(28), textAlign: 'center', fontFamily: 'Roboto' }}>Charges</Text>
                        </View>
                        <View style={{ backgroundColor: '#FEF9E3', width: '90%', height: normalizeH(450), padding: 10, marginTop: normalizeH(-20), zIndex: 0 }}>
                            <View style={{ flex: 4, }}>
                                <ScrollView style={{ width: '100%', marginTop: normalizeH(30), paddingRight: 7 }}>
                                    {
                                        chkout.ready && chkout.data.length>0 && chkout.data.map((item,idx)=>{
                                            return(
                                                <ChargeItem key={idx} drink={item.drink} drinkqty={item.drinkqty} 
                                                side={item.side} sideqty={item.sideqty} sidesname={item.sidename} drinksname={item.drinksname} restaurantname={item.restaurantname} 
                                                price={item.price}  title={item.title}
                                                />
                                            )
                                        })
                                    }
                                    <ChargeItem key={'fee'} title={'Service Fee'} price={100} restaurantname={'Passchru'} />
                                </ScrollView>
                            </View>
                            <View style={{ flex: 1, }}>
                                <Image source={require('../assets/images/checkoutDivider.png')} style={{ marginVertical: normalizeH(10) }} />
                                <View style={{ marginLeft: normalizeW(20), flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={{ fontSize: normalizeW(32), fontFamily: 'RobotoLight' }}>Total</Text>
                                    <Image source={require('../assets/images/totalDot.png')} style={{ marginHorizontal: 10, width: 15, height: 15 }} />
                                    <Text style={{ fontSize: normalizeW(32), fontFamily: 'RobotoLight' }}>${numberWithCommas(sum+100)}.00</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{width:'90%',flexDirection:'column',alignItems:'flex-start',justifyContent:'flex-start',}}>
                            <PaymentOptions payment={payment} setPayment={setPayment} />
                            <PickUp pickup={pickup} setPickup={setPickup} />
                            <View style={{alignSelf:'center',alignItems:'center',width:'100%',marginTop:normalizeH(40)}}>
                            <TouchableOpacity activeOpacity={0.85} onPress={()=>{submitOrder()}}>
                            <View style={{backgroundColor:'#000',paddingHorizontal:normalizeW(10),paddingVertical:normalizeH(5),borderRadius:10}}>
                                <Text style={{fontFamily:'RobotoLight',fontSize:normalizeW(32),letterSpacing:1.2,color:'#fff'}}>CONFIRM ORDER</Text>
                            </View>
                            </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </View>
        </Base>
    )
}
